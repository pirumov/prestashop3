package myprojects.automation.lesson3.tests;

import myprojects.automation.lesson3.BaseScript;
import myprojects.automation.lesson3.GeneralActions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class CreateCategoryTest extends BaseScript {



    public static void main(String[] args) throws InterruptedException {
        // TODO prepare driver object
        EventFiringWebDriver driver = getConfiguredDriver();

        GeneralActions actions = new GeneralActions(driver, "New Category");
        actions.login("webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw");

        // create category

        actions.createCategory();

        // check that new category appears in Categories table
        actions.checkCategoryAppears();

        // finish script
        quitDriver(driver);
    }
}
