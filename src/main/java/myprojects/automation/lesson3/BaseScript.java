package myprojects.automation.lesson3;

import myprojects.automation.lesson3.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;


public abstract class BaseScript {

    private static WebDriver getDriver() {
        String browser = Properties.getBrowser();
        String os = Properties.getOS();

        //OS check only for use drivers for Mac or Windows

        if (!os.equals("Mac OS X")) {
            switch (browser) {
                case "firefox":
                    System.setProperty(
                            "webdriver.gecko.driver",
                            new File(BaseScript.class.getResource("/geckodriver.exe").getFile()).getPath());
                    return new FirefoxDriver();
                case "ie":
                case "internet explorer":
                    System.setProperty(
                            "webdriver.ie.driver",
                            new File(BaseScript.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                    return new InternetExplorerDriver();
                case "chrome":
                default:
                    System.setProperty(
                            "webdriver.chrome.driver",
                            new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
                    return new ChromeDriver();
            }
        } else {
            switch (browser) {
                case "firefox":
                    System.setProperty(
                            "webdriver.gecko.driver",
                            new File(BaseScript.class.getResource("/geckodriver").getFile()).getPath()); //MacOS
                    return new FirefoxDriver();
                case "chrome":
                default:
                    System.setProperty(
                            "webdriver.chrome.driver",
                            new File(BaseScript.class.getResource("/chromedriver").getFile()).getPath()); //MacOS
                    return new ChromeDriver();
            }
        }
    }


    public static EventFiringWebDriver getConfiguredDriver() {

        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);

        wrappedDriver.register(new EventHandler());

        return wrappedDriver;
    }

    public static void quitDriver(WebDriver driver){
        driver.quit();
    }
}
