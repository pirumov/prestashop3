package myprojects.automation.lesson3;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;

import static com.sun.tools.doclets.formats.html.markup.HtmlStyle.title;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private String categoryName;

    private WebElement loginField;
    private WebElement passwordField;

    private WebElement catalog;

    private WebElement filteredRow;

    public GeneralActions(EventFiringWebDriver driver, String categoryName) {
        this.driver = driver;
        this.categoryName = categoryName;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        // TODO implement logging in to Admin Panel

        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");

        // Form Fields initialize and submit
        loginField = driver.findElement(By.id("email"));
        passwordField = driver.findElement(By.name("passwd"));
        loginField.sendKeys("webinar.test@gmail.com");
        passwordField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        passwordField.submit();
    }

    /**
     * Adds new category in Admin Panel.
     */
    public void createCategory() throws InterruptedException {
        // TODO implement logic for new category creation

        catalog = driver.findElement(By.id("subtab-AdminCatalog"));

        Actions actions = new Actions(driver);
        actions.moveToElement(catalog).build().perform();

        wait.until(ExpectedConditions. visibilityOf(catalog.findElements(By.cssSelector("li")).get(1)));
        catalog.findElements(By.cssSelector("li")).get(1).click();

        WebElement addCategory = driver.findElement(By.id("desc-category-new"));
        addCategory.click();

        WebElement categoryNameInput = driver.findElement(By.id("name_1"));
        WebElement buttonSave = driver.findElement(By.id("category_form_submit_btn"));

        categoryNameInput.sendKeys(categoryName);
        buttonSave.click();

        // Check for pop-up system notification shown
        wait.until((Predicate) webDriver -> driver.findElement(By.cssSelector(".alert.alert-success")).getText().contains("Создано"));
        if (!driver.findElement(By.cssSelector(".alert.alert-success")).getText().contains("Создано")){
            System.out.println("System message isn't shown");
        }

    }

    public void checkCategoryAppears() {

        WebElement sortByName = driver.findElement(By.name("categoryFilter_name"));

        sortByName.sendKeys(categoryName);
        WebElement sortBtn = driver.findElement(By.name("submitFilter"));
        sortBtn.click();

        waitForCategory();
    }



    private void waitForCategory() {
        wait.until((Predicate) webDriver -> driver.findElement(By.xpath("//*[@id=\"table-category\"]/tbody/tr"))
                .findElements(By.cssSelector("td")).get(2).getText().equals(categoryName) );
    }


}
